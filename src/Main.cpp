#include <iostream>
#include <fstream>
#include <cstring>

#include <chrono>
#include <cassert>
#include <thread>

#include <sys/file.h>
#include <unistd.h>

#include "Camera.h"

#define SLEEP(ms) std::this_thread::sleep_for(std::chrono::milliseconds(ms));


void log(std::string msg, int num=-1) {
    auto time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
    auto seconds = time.count() / 1000.0 / 1000.0;
    std::cout << std::fixed << seconds << ':' << msg;

    if(num >= 0) {
        std::cout << ':' << num;
    }

    std::cout << std::endl;
}


bool write_buffer(const int fd, uint8_t* b, const size_t s, const int retry_on_interrupt)
{
  size_t n = s;
  while (0 < n) {
    ssize_t result = write(fd, b, n);
    if (-1 == result) {
      if ((retry_on_interrupt && (errno == EINTR)) || (errno == EWOULDBLOCK) || (errno == EAGAIN)) {
        continue;
      }
      else {
        break;
      }
    }

    n -= result;
    b += result;
  }

  return (0 < n) ? false : true;
}


bool write_data(uint8_t* data, size_t size, uint32_t num, std::string filename) {
    auto fd = fopen(filename.c_str(), "wb");
    if(fd == NULL) {
        return false;
    }

    auto fdno = fileno(fd);
    auto lck = flock(fdno, LOCK_EX | LOCK_NB);
    bool to_ret = true;
    if(lck == 0) {
        to_ret |= write_buffer(fdno, (uint8_t*)&num, sizeof(uint32_t), true);
        to_ret |= write_buffer(fdno, data, size, true);
        flock(fdno, LOCK_UN);

	if(!to_ret) {
	    log("write-out");
	}
    }

    else {
	log("locked-out");
	to_ret = false;
    }

    fclose(fd);
    return to_ret;
}

int main(int argc, char* argv[])
{
    assert(argc == 3);
    double start_after = std::atoi(argv[1]);
    std::string file_out = argv[2];

    auto time_0 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());

    uint16_t width = 960;
    uint16_t height = 720;
    int repetitions = -1;

    int fps = 33;
    uint8_t* cam_data;

    Camera cam(width, height, fps);
    cam.open();

    while(true) {
        auto time = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch());
        double seconds = (time - time_0).count() / 1000.0 / 1000.0;
        if(seconds > start_after) {
    	    break;
        }
        SLEEP(100);
    }
    std::cerr << "START: " << argv[0] << std::endl;

    for(int i = 0; repetitions == -1 || i < repetitions; i++)
    {
        log("start", i);
        cam_data = cam.retrieve();
	bool ok = write_data(cam_data, width * height, i, file_out);
	if(!ok) {
	    continue;
	}

	log("end", i);
    }

    return 0;
}
