#ifndef CAMERA_H
#define CAMERA_H

#include <raspicam/raspicam.h>

#define CAMERA_INIT_SLEEP_TIME 2000

class Camera
{
public:
    Camera(uint32_t width, uint32_t height, uint32_t frame_rate);
    virtual ~Camera();

    // call after constructor, check return type for success.
    bool open();
    // retrieve image from camera
    uint8_t* retrieve();
    // retrieve image size from camera
    size_t get_size();

private:
    raspicam::RaspiCam m_camera;
    raspicam::RASPICAM_FORMAT m_format;
    bool m_opened;
};

#endif // CAMERA_H
