#include "Camera.h"

#include <iostream>
#include <chrono>
#include <thread>

Camera::Camera(uint32_t width, uint32_t height, uint32_t frame_rate)
{
    m_format = raspicam::RASPICAM_FORMAT_GRAY;
    m_camera.setFormat(m_format);
    m_camera.setImageEffect(raspicam::RASPICAM_IMAGE_EFFECT_BLUR);

    // configurable stuff
    m_camera.setCaptureSize(width, height);
    m_camera.setFrameRate(frame_rate);

    // currently unconfigurable stuff, still to optimize
    m_camera.setAWB(raspicam::RASPICAM_AWB_OFF);
    m_camera.setExposure(raspicam::RASPICAM_EXPOSURE_FIXEDFPS);
    m_camera.setISO(200);

    m_opened = false;
}

Camera::~Camera()
{
    m_camera.release();
}

bool Camera::open()
{
    if(!m_camera.open())
    {
        std::cerr << "Error opening the camera..." << std::endl;
        return false;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(CAMERA_INIT_SLEEP_TIME));
    m_opened = true;
    return true;
}

uint8_t* Camera::retrieve()
{
    if(!m_opened)
    {
        std::cerr << "Retrieving from unopened camera..." << std::endl;
        return nullptr;
    }

    m_camera.grab();
    return m_camera.getImageBufferData();
}

size_t Camera::get_size()
{
    return m_opened ? m_camera.getImageTypeSize(m_format) : 0;
}
