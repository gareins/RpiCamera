#include "Settings.h"

void throw_bad_ini(const std::string& section, const std::string& key)
{
    throw std::runtime_error("Bad section -- key: " + section + " -- " + key);
}

template<class T>
T get_num(INIReader& ini, const std::string& section, const std::string& key)
{
    T to_ret = ini.GetInteger(section, key, -10000); // one cannot use values less then -1000
    if(to_ret < -1000)
    {
        throw_bad_ini(section, key);
    }
    return to_ret;
}

Settings::Settings(const std::string& file_name)
{
    inifile = INIReader(file_name);
}

bool Settings::get_bool(const std::string& section, const std::string& key) {
    std::string to_ret = inifile.Get(section, key, "bad");
    std::transform(to_ret.begin(), to_ret.end(), to_ret.begin(), ::tolower);

    if(to_ret == "false")
    {
        return false;
    }
    else if (to_ret == "true")
    {
        return true;
    }
    else
    {
        throw_bad_ini(section, key);
        return false;
    }
}

std::string Settings::get_string(const std::string& section, const std::string& key)
{
    std::string to_ret = inifile.Get(section, key, "bad");
    if(to_ret == "bad")
    {
        throw_bad_ini(section, key);
    }
    return to_ret;
}

int Settings::get_int(const std::string& section, const std::string& key)
{
    return get_num<int>(inifile, section, key);
}

float Settings::get_float(const std::string& section, const std::string& key)
{
    return get_num<float>(inifile, section, key);
}
