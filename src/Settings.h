#include "IniReader.h"

class Settings {
public:
    Settings(const std::string& filename);

    bool get_bool(const std::string& section, const std::string& key);
    int get_int(const std::string& section, const std::string& key);
    float get_float(const std::string& section, const std::string& key);
    std::string get_string(const std::string& section, const std::string& key);

private:
    INIReader inifile;
};
