#include "ImageProcess.cpp"
#include <fstream>

int main() {
    uint16_t width = 192;
    uint16_t height = 145;
    uint8_t threshold = 128;
    uint8_t* data = (uint8_t*)malloc(width * height);

    const int max = 256;
    const int a = 13;
    const int c = 251;
    int value = 0;
    for(int i = 0; i < width * height; i++) {
        value = (a * value + c) % max;
        data[i] = value;        
    }

    std::ofstream beforeFile("img_before.ppm", std::ios::binary );
    beforeFile << "P5\n" << int(width) << " " << int(height) << " 255\n";
    beforeFile.write( ( char* ) data, width * height );

    ImageProcess packer(width, height, threshold);
    uint8_t* img_small = packer.pack_image(data);
    
    ImageProcess unpacker(width, height, threshold);
    uint8_t* img_back = unpacker.unpack_image(img_small);

    std::ofstream afterFile("img_after.ppm", std::ios::binary );
    afterFile << "P5\n" << int(width) << " " << int(height) << " 255\n";
    afterFile.write( ( char* ) img_back, width * height );

    for(int i = 0; i < width * height; i++) {
        uint8_t data_thresh = (data[i] > threshold) * 255;
        if(img_back[i] != data_thresh) {
            std::cout << "ERR @ " << i << std::endl;
        }
    }
    return 0; 
}