# RaspiCam

This project is C++ project to obtain and process images from raspberrypi camera
and send them over the network, with as little latency as possible.

It uses blurring and thresholding to process the image, uleb128 and run length
encoding are used to pack the image and TODO zeromq is used to send it to
the reciever.

settings.ini file is used for settings. Please do not commit changes to this file 
unless you know why.

Thanks to the guys, who provided IniReader and Uleb128 encoder. More info
in appropriate header files.

# Requirements

To install you need:

* git to download this repo
* cmake
* g++
* raspicam (I use version from github.com/cedricve/raspicam)
* libzmq-dev
* mmal library, that ships with raspberian

# Instalation

Just use classic cmake procedure:

* mkdir build
* cd build
* cmake ..
* make

To build profile optimized version, run scripts/pgo.sh
