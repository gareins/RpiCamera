#!/bin/bash

function prnt {
	printf "**** %-60s ****\n" "$1"
}

cd $(dirname $(readlink -f $0))

TARGET="./build/RpiCamera"
THRESHOLD=$(grep "^threshold=" ../settings.ini | cut -d'=' -f2)
SKIP_PIXEL=$(grep "^pixel_diff=" ../settings.ini | cut -d'=' -f2)

DEFINES="-DPGO -DTHRESHOLD=$THRESHOLD -DSKIP_PIXEL=$SKIP_PIXEL"
prnt "target = $TARGET, threshold = $THRESHOLD, pixel_skipping=$SKIP_PIXEL"

cd ..
rm -rf build
mkdir build
cd build

prnt "Profile data generation CMAKE"
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-fprofile-generate $DEFINES"
prnt "Profile data generation Gnu make"
make -j4

prnt "Profile data generation run"
cd ..
sudo $TARGET
cd build

prnt "Profile data use CMAKE"
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-fprofile-use $DEFINES"
prnt "Profile data use Gnu make"
make -j4


prnt "DONE"
prnt "BEWARE: PGO build does not read threshold from settings.ini"
prnt "        at runtime but at build time! Also bound check at"
prnt "        image packing is no longer performed (BUFFER_SIZE)"