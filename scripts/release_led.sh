#!/bin/bash

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit
fi

cd $(dirname $(readlink -f $0))
gpio=$(grep "gpio=" ../settings.ini | cut -f2 -d'=')

echo "$gpio" > /sys/class/gpio/unexport
if [ "$?" -ne 0 ]; then
  echo "Camera already released"
else
  echo "Success."
fi